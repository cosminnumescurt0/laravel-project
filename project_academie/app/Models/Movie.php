<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'genre', 'release_date', 'duration', 'language', 'director_id'];

    public function director() {
        return $this->belongsTo(Director::class);
    }

    public function profit() {
        return $this->hasOne(Profit::class);
    }

    public function actors() {
        return $this->belongsToMany(Actor::class, 'casts')->withPivot('role');
    }
}
