<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    use HasFactory;

    protected $fillable = ['role', 'movie_id', 'actor_id'];

    public function movie() {
        return $this->belongsToMany(Movies::class);
    }

    public function actor() {
        return $this->hasMany(Actors::class);
    }
}
