<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profit extends Model
{
    use HasFactory;

    protected $fillable = ['costs', 'budget', 'gains', 'movie_id'];

    public function profit() {
        return $this->hasMany(Movies::class);
    }
}
