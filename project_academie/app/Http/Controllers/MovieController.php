<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movie = Movie::all();
        return [
            'data'=> $movie
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated =  $request->validate([
                'director_id' => 'required',
                'title' => 'required',
                'description' => 'required',
                'genre' => 'required',
                'release_date' => 'required',
                'duration' => 'required',
                'language' => 'required'
            ]);
    
            $movie = movie::create($validated);
    
            return [
                'data' => $movie
            ];

        }catch (\Exception $e){
            return $e-> getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $movie = Movie::find($id);

        if (is_null($movie))
        {
            return [
                'data' => 'Not Found'
            ];
        }

        return [
            'data' => $movie
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $movie = Movie::find($id);
        if (is_null($movie))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $movie -> update($request->all());
        return [
            'data' => $movie
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $movie = Movie::find($id);
        if (is_null($movie))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $movie -> delete();
        return [
            'data' => 'Succes!'
        ];
    }
}
