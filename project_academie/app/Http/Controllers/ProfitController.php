<?php

namespace App\Http\Controllers;

use App\Models\Profit;
use Illuminate\Http\Request;

class ProfitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profit = Profit::all();
        return [
            'data'=> $profit
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated =  $request->validate([
                'costs' => 'required|int',
                'budget' => 'required|int',
                'gains' => 'required|int',
                'movie_id' => 'required',
            ]);
    
            $profit = Profit::create($validated);
    
            return [
                'data' => $profit
            ];

        }catch (\Exception $e){
            return $e-> getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profit  $profit
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $profit = Profit::find($id);

        if (is_null($profit))
        {
            return [
                'data' => 'Not Found'
            ];
        }

        return [
            'data' => $profit
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profit  $profit
     * @return \Illuminate\Http\Response
     */
    public function edit(Profit $profit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profit  $profit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $profit = Profit::find($id);
        if (is_null($profit))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $profit -> update($request->all());
        return [
            'data' => $profit
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profit  $profit
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $profit = Profit::find($id);
        if (is_null($profit))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $profit -> delete();
        return [
            'data' => 'Succes!'
        ];
    }
}
