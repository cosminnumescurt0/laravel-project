<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = Cast::all();
        return [
            'data'=> $cast
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated =  $request->validate([
                'costs' => 'required|int',
                'budget' => 'required|int',
                'gains' => 'required|int',
                'movie_id' => 'required',
            ]);
    
            $cast = cast::create($validated);
    
            return [
                'data' => $cast
            ];

        }catch (\Exception $e){
            return $e-> getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $cast = Cast::find($id);

        if (is_null($cast))
        {
            return [
                'data' => 'Not Found'
            ];
        }

        return [
            'data' => $cast
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function edit(Cast $cast)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $cast = Cast::find($id);
        if (is_null($cast))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $cast -> update($request->all());
        return [
            'data' => $cast
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $cast = Cast::find($id);
        if (is_null($cast))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $cast -> delete();
        return [
            'data' => 'Succes!'
        ];
    }
}
