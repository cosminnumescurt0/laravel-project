<?php

namespace App\Http\Controllers;

use App\Models\Director;
use Illuminate\Http\Request;

class DirectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $director = Director::all();
        return [
            'data'=> $director
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated =  $request->validate([
                'first_name' => 'required|string|min:3',
                'last_name' => 'required|string|min:3',
                'birth' => 'required',
                'town' => 'required', 
                'country' => 'required'
            ]);
    
            $director = Director::create($validated);
    
            return [
                'data' => $director
            ];

        }catch (\Exception $e){
            return $e-> getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $director = Director::find($id);

        if (is_null($director))
        {
            return [
                'data' => 'Not Found'
            ];
        }

        return [
            'data' => $director
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function edit(Director $director)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $director = Director::find($id);
        if (is_null($director))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $director -> update($request->all());
        return [
            'data' => $director
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Director  $director
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $director = Director::find($id);
        if (is_null($director))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $director -> delete();
        return [
            'data' => 'Succes!'
        ];
    }
}
