<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actor = Actor::all();
        return [
            'data'=> $actor
        ];
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validated =  $request->validate([
                'first_name' => 'required|string|min:3',
                'last_name' => 'required|string|min:3',
                'birth' => 'required',
                'gender' => 'required',
                'description' => 'required',
                'town' => 'required', 
                'country' => 'required'
            ]);
    
            $actor = actor::create($validated);
    
            return [
                'data' => $actor
            ];

        }catch (\Exception $e){
            return $e-> getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function show(Actor $actor)
    {
        $actor = Actor::find($id);

        if (is_null($actor))
        {
            return [
                'data' => 'Not Found'
            ];
        }

        return [
            'data' => $actor
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function edit(Actor $actor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Actor $actor)
    {
        $actor = Actor::find($id);
        if (is_null($actor))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $actor -> update($request->all());
        return [
            'data' => $actor
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Actor  $actor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actor $actor)
    {
        $actor = Actor::find($id);
        if (is_null($actor))
            {
                return [
                    'data' => 'Not Found',
                    'success' => false
                ];
            }

        $actor -> delete();
        return [
            'data' => 'Succes!'
        ];
    }
}
