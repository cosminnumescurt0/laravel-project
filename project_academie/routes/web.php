<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

   
Route::namespace('App\Http\Controllers')->group( function() {

    /* Directors table operations*/

    Route::get('directors', 'DirectorController@index');
    Route::get('directors/{id}', 'DirectorController@show');
    Route::post('directors', 'DirectorController@store');
    Route::put('directors/{id}', 'DirectorController@update');
    Route::delete('directors/{id}', 'DirectorController@destroy');

         /* Actors table operations*/

    Route::get('actors', 'ActorController@index');
    Route::get('actors/{id}', 'ActorController@show');
    Route::post('actors', 'ActorController@store');
    Route::put('actors/{id}', 'ActorController@update');
    Route::delete('actors/{id}', 'ActorController@destroy');

         /* Movies table operations*/

    Route::get('movies', 'MovieController@index');
    Route::get('movies/{id}', 'MovieController@show');
    Route::post('movies', 'MovieController@store');
    Route::put('movies/{id}', 'MovieController@update');
    Route::delete('movies/{id}', 'MovieController@destroy');

         /* Casts table operations*/

    Route::get('casts', 'CastController@index');
    Route::get('casts/{id}', 'CastController@show');
    Route::post('casts', 'CastController@store');
    Route::put('casts/{id}', 'CastController@update');
    Route::delete('casts/{id}', 'CastController@destroy');

         /* Profits table operations*/

    Route::get('profits', 'ProfitController@index');
    Route::get('profits/{id}', 'ProfitController@show');
    Route::post('profits', 'ProfitController@store');
    Route::put('profits/{id}', 'ProfitController@update');
    Route::delete('profits/{id}', 'ProfitController@destroy');

});